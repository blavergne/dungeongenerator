
public class Coordinate {

	int xPos;
	int yPos;
	
	Coordinate(int xPos, int yPos){
		this.xPos = xPos;
		this.yPos = yPos;
	}
	
	public void setRowPosition(int val) {
		xPos = val;
	}
	
	public void setColPosition(int val) {
		yPos = val;
	}
	
	
	
}
