import java.util.Random;

public class Room {

	private Coordinate roomPosition;
	private Coordinate doorPosition;
	private final int roomHeight;
	private final int roomWidth;
	private char direction;
	private int minRoomHeight = 4;
	private int maxRoomHeight = 6;
	private int minRoomWidth = 4;
	private int maxRoomWidth = 6;
	
	Room(Coordinate roomPosition, int height, int width, char direction){
		this.roomPosition = roomPosition;
		this.roomHeight = height;
		this.roomWidth = width;
		this.direction = direction;
		doorPosition = new Coordinate(0, 0);
		addRoomToMap();
	}
	
	private void addRoomToMap() {
	
		Random rand = new Random();
		int roomPosX = -1;
		int roomPosY = -1;
		int randDoorPos = -1;
		
		if(direction == 'w') {
			System.out.println("WEST");
			roomPosX = roomPosition.xPos -= roomWidth;
			roomPosY = roomPosition.yPos - (rand.nextInt(roomHeight - 2) + 1);
		} else if(direction == 'n') {
			System.out.println("NORTH");
			roomPosX = roomPosition.xPos - (rand.nextInt(roomWidth - 2) + 1);
			roomPosY = roomPosition.yPos -= roomHeight;
		} else if(direction == 's') {
			System.out.println("SOUTH");
			roomPosX = roomPosition.xPos - (rand.nextInt(roomWidth - 2) + 1);;
			roomPosY = roomPosition.yPos;
		} else {
			System.out.println("EAST");
			roomPosX = roomPosition.xPos;
			roomPosY = roomPosition.yPos - (rand.nextInt(roomHeight - 2) + 1);
		}
		
		if(legalRoom(roomPosX, roomPosY)) {
			for(int i = (0 + roomPosY); i < roomPosY + roomHeight; i ++) {
				for(int j = roomPosX; j < roomPosX + roomWidth; j++) {
					if(i == roomPosY || i == roomPosY + roomHeight - 1) {
						if(Map.dungeonMap[i][j] == '.')
							Map.dungeonMap[i][j] = '#';
					}
					if(j == roomPosX || j == roomPosX + roomWidth - 1) {
						if(Map.dungeonMap[i][j] == '.')
							Map.dungeonMap[i][j] = '#';
					}
				}	
			}
		
			if(rand.nextInt(2) > 0) {
				if(direction != 's') {
					direction = 'n';
					randDoorPos = rand.nextInt(roomWidth - 2) + roomPosX + 1;
					setDoorPosition(randDoorPos, roomPosY);
					System.out.println("X: " + roomPosY + " Y: " + randDoorPos + " Facing: " + direction);
					Map.dungeonMap[roomPosY][randDoorPos] = '=';
					Map.dungeonMap[roomPosY - 1][randDoorPos] = '=';
				}
				else {
					direction = 's';
					randDoorPos = rand.nextInt(roomWidth - 2) + roomPosX + 1;
					setDoorPosition(randDoorPos, roomPosY + (roomHeight - 1) + 1);
					System.out.println("X: " + roomPosY + " Y: " + randDoorPos + " Facing: " + direction);
					Map.dungeonMap[roomPosY + (roomHeight - 1)][randDoorPos] = '=';
					Map.dungeonMap[roomPosY + (roomHeight - 1) + 1][randDoorPos] = '=';
				}
			}
			else
				if(direction != 'e') {
					direction = 'w';
					randDoorPos = rand.nextInt(roomHeight - 2) + roomPosY + 1;
					setDoorPosition(roomPosX, randDoorPos);
					System.out.println("X: " + roomPosY + " Y: " + randDoorPos + " Facing: " + direction);
					Map.dungeonMap[randDoorPos][roomPosX] = '=';
					Map.dungeonMap[randDoorPos][roomPosX - 1] = '=';
				}
				else {
					direction = 'e';
					randDoorPos = rand.nextInt(roomHeight - 2) + roomPosY + 1;
					setDoorPosition( roomPosX + (roomWidth - 1) + 1, randDoorPos);
					System.out.println("X: " + roomPosY + " Y: " + randDoorPos + "	Facing: " + direction);
					Map.dungeonMap[randDoorPos][roomPosX + (roomWidth - 1)] = '=';
					Map.dungeonMap[randDoorPos][roomPosX + (roomWidth - 1) + 1] = '=';
				}
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			int newRoomHeight = rand.nextInt(maxRoomHeight - minRoomHeight) + minRoomHeight;
			int newRoomWidth = rand.nextInt(maxRoomWidth - minRoomWidth) + minRoomWidth;
			
			Map.drawMapToConsole();
			new Room(doorPosition, newRoomHeight, 6, direction);
		}
	}
	
	public boolean legalRoom(int roomPosX, int roomPosY) {
		
		if(roomPosX > Map.mapWidth || roomPosX < 0 || roomPosX + roomWidth > Map.mapWidth)
			return false;
		if(roomPosY > Map.mapHeight || roomPosY < 0 || roomPosY + roomHeight > Map.mapHeight)
			return false;
		
		for(int i = (0 + roomPosY); i < roomPosY + roomHeight; i ++) {
			for(int j = roomPosX; j < roomPosX + roomWidth; j++) {
					if(Map.dungeonMap[i][j] == '#')
						return false;
			}	
		}
		
		return true;
	}
	
	public void setDoorPosition(int xPos, int yPos) {
		
		this.doorPosition.xPos = xPos;
		this.doorPosition.yPos = yPos;
		
	}
	
	public Coordinate getRoomPosition() {
		return roomPosition;
	}
	
	public Coordinate getDoorPosition() {
		return doorPosition;
	}
	
	public int getRoomHeight() {
		return roomHeight;
	}
	
	public int getRoomWidth() {
		return roomWidth;
	}
	
}
