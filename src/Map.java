import java.util.Random;

public class Map {

	static int mapHeight = 25;
	static int mapWidth = 100;
	public static char[][] dungeonMap = new char[mapHeight][mapWidth];
	
	Map(){
		
		instantiateMap();
		
	}
	
	public char[][] getDungeonMap() {
		return dungeonMap;
	}
	
	private void instantiateMap() {
		for(int i = 0; i < mapHeight;  i++) {
			for(int j = 0; j < mapWidth;  j++)
				dungeonMap[i][j] = '.';
		}
	}
	
	public static void drawMapToConsole() {
		for(int i = 0; i < mapHeight;  i++) {
			for(int j = 0; j < mapWidth;  j++)
				System.out.print(dungeonMap[i][j]);
			
			System.out.println();
		}
	}
	
}
